package drools_test

import (
	"fmt"
	"go-drools"
	"strings"
	"testing"
)

func printKnowledge(k *drools.Knowledge) {
	for i0, r := range k.Rules {
		fmt.Printf("Rule[%d] -> %s\n", i0, r.Name)
		for i1, m := range r.Metas {
			fmt.Printf("\tMeta[%d] -> %s(%s)\n", i1, m.Name, strings.Join(m.Parameters, ", "))
		}
		for i1, p := range r.Predicates {
			fmt.Printf("\tFact[%d] -> %s\n", i1, p.Name)
			for i2, e := range p.Expressions {
				fmt.Printf("\t\tExpr[%d] -> %s %s %s\n", i2, e.Lhs, drools.TokenToString(e.Op), e.Rhs)
			}
		}
	}
}

func TestParser_ParseKnowledge_SingleRule(t *testing.T) {
	s := `rule "Hello World"
	    when
		    Message1($text: text, id == 0)
			Message2(text == $text)
			Message3($text2 : text == $text)
		then
		    System.out.print(text);
		end`
	p := drools.NewParser(strings.NewReader(s)) //.ParseQuery()
	knowledge, err := p.ParseString(strings.NewReader(s))

	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if knowledge == nil {
		t.Fatal("unexpected Knowledge is nil")
	} else if len(knowledge.Rules) != 1 {
		t.Fatalf("unexpected rules count: %d", len(knowledge.Rules))
	} else if knowledge.Rules[0].Name != "Hello World" {
		t.Fatalf("unexpected name: %s", knowledge.Rules[0].Name)
	} else if len(knowledge.Rules[0].Predicates) != 3 {
		t.Fatalf("unexpected predicates count: %d", len(knowledge.Rules[0].Predicates))
	}

	printKnowledge(knowledge)
}

func TestParser_ParseKnowledge_RuleMeta(t *testing.T) {
	s := `rule "Hello World"
		@oldName("Hello From Heaven")
		@oldName("Hello From Heaven2")
		@oldName("Hello From Heaven3")
	    when
		    Message1($text: text, id == 0)
			Message2(text == $text)
			Message3($text2 : text == $text)
		then
		    System.out.print(text);
		end`
	p := drools.NewParser(strings.NewReader(s)) //.ParseQuery()
	knowledge, err := p.ParseString(strings.NewReader(s))

	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if knowledge == nil {
		t.Fatal("unexpected Knowledge is nil")
	} else if len(knowledge.Rules) != 1 {
		t.Fatalf("unexpected rules count: %d", len(knowledge.Rules))
	} else if knowledge.Rules[0].Name != "Hello World" {
		t.Fatalf("unexpected name: %s", knowledge.Rules[0].Name)
	} else if len(knowledge.Rules[0].Predicates) != 3 {
		t.Fatalf("unexpected predicates count: %d", len(knowledge.Rules[0].Predicates))
	} else if len(knowledge.Rules[0].Metas) != 1 {
		t.Fatalf("unexpected metas count: %d", len(knowledge.Rules[0].Metas))
	}

	printKnowledge(knowledge)
}

func TestParser_ParseKnowledge_MultipleRules(t *testing.T) {
	s := `rule "Hello World"
	    when
		    Message1($text: text, id == 0)
			Message2(text == $text)
			Message3($text2 : text == $text)
		then
		    System.out.print(text);
		end
		
		rule "Hello World2"
	    when
		    Message4($text: text, id == 1)
			Message5(text == $text)
			Message6($text2 : text == $text)
		then
		    System.out.print(text);
		end`
	p := drools.NewParser(strings.NewReader(s)) //.ParseQuery()
	knowledge, err := p.ParseString(strings.NewReader(s))

	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if knowledge == nil {
		t.Fatal("unexpected Knowledge is nil")
	} else if len(knowledge.Rules) != 2 {
		t.Fatalf("unexpected rules count: %d", len(knowledge.Rules))
	} else if knowledge.Rules[0].Name != "Hello World" {
		t.Fatalf("unexpected name: %s", knowledge.Rules[0].Name)
	} else if len(knowledge.Rules[0].Predicates) != 3 {
		t.Fatalf("unexpected predicates count: %d", len(knowledge.Rules[0].Predicates))
	}

	printKnowledge(knowledge)
}
