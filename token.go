package drools

import (
	"strings"
)

type Token int

const (
	ILLEGAL Token = iota
	EOF
	WS

	litteral_beg
	IDENT      // main
	NUMBER     // 152
	STRING     // "abc" or 'abc'
	BADSTRING  // "abc
	BADESCAPE  // \q
	TRUE       // true
	FALSE      // false
	NULL       // null
	META_IDENT // @key
	litteral_end

	operator_beg
	// Operators
	ADD // +
	SUB // -
	MUL // *
	DIV // /

	AND // AND
	OR  // OR

	EQ    // =
	NEQ   // !=
	LT    // <
	LTE   // <=
	GT    // >
	GTE   // >=
	COMMA // ,
	COLON // :
	DOT   // .

	operator_end

	LPAREN    // (
	RPAREN    // )
	SEMICOLON // ;
	COMMENT   // //
	LCOMMENT  // /*
	RCOMMENT  // */

	keyword_beg

	ACCUMULATE
	ACTION
	ATTRIBUTES
	COLLECT
	DECLARE
	DIALECT
	END
	ENTRY_POINT
	EVAL
	EXISTS
	EXTEND
	FORALL
	FROM
	FUNCTION
	GLOBAL
	IMPORT
	IN
	INIT
	NOT
	OVER
	PACKAGE
	QUERY
	RESULT
	REVERSE
	RULE
	TEMPLATE
	THEN
	WHEN
	keyword_end

	rule_attribute_beg
	ACTIVATION_GROUP
	AGENDA_GROUP
	AUTO_FOCUS
	DATE_EFFECTIVE
	DATE_EXPIRES
	DURATION
	ENABLED
	LOCK_ON_ACTIVE
	NO_LOOP
	RULEFLOW_GROUP
	SALIENCE
	rule_attribute_end
)

var tokens = [...]string{
	ILLEGAL: "ILLEGAL",
	EOF:     "EOF",
	WS:      "WS",

	IDENT:      "IDENT",
	NUMBER:     "NUMBER",
	STRING:     "STRING",
	BADSTRING:  "BADSTRING",
	BADESCAPE:  "BADESCAPE",
	TRUE:       "TRUE",
	FALSE:      "FALSE",
	NULL:       "null",
	META_IDENT: "META_IDENT",

	ADD: "+",
	SUB: "-",
	MUL: "*",
	DIV: "/",

	AND: "AND",
	OR:  "OR",

	EQ:  "==",
	NEQ: "!=",
	LT:  "<",
	LTE: "<=",
	GT:  ">",
	GTE: ">=",

	COMMA: ",",
	COLON: ":",
	DOT:   ".",

	LPAREN:    "(",
	RPAREN:    ")",
	SEMICOLON: ";",
	COMMENT:   "//",
	LCOMMENT:  "/*",
	RCOMMENT:  "*/",

	ACCUMULATE:       "ACCUMULATE",
	ACTION:           "ACTION",
	ACTIVATION_GROUP: "ACTIVATION-GROUP",
	AGENDA_GROUP:     "AGENDA-GROUP",
	ATTRIBUTES:       "ATTRIBUTES",
	AUTO_FOCUS:       "AUTO-FOCUS",
	COLLECT:          "COLLECT",
	DATE_EFFECTIVE:   "DATE-EFFECTIVE",
	DATE_EXPIRES:     "DATE-EXPIRES",
	DECLARE:          "DECLARE",
	DIALECT:          "DIALECT",
	DURATION:         "DURATION",
	ENABLED:          "ENABLED",
	END:              "END",
	ENTRY_POINT:      "ENTRY_POINT",
	EVAL:             "EVAL",
	EXISTS:           "EXISTS",
	EXTEND:           "EXTEND",
	FORALL:           "FORALL",
	FROM:             "FROM",
	FUNCTION:         "FUNCTION",
	GLOBAL:           "GLOBAL",
	IMPORT:           "IMPORT",
	IN:               "IN",
	INIT:             "INIT",
	LOCK_ON_ACTIVE:   "LOCK-ON-ACTIVE",
	NO_LOOP:          "NO-LOOP",
	NOT:              "NOT",
	OVER:             "OVER",
	PACKAGE:          "PACKAGE",
	QUERY:            "QUERY",
	RESULT:           "RESULT",
	REVERSE:          "REVERSE",
	RULE:             "RULE",
	RULEFLOW_GROUP:   "RULEFLOW-GROUP",
	SALIENCE:         "SALIENCE",
	TEMPLATE:         "TEMPLATE",
	THEN:             "THEN",
	WHEN:             "WHEN",
}

var keywords map[string]Token

func init() {
	keywords = make(map[string]Token)
	for tok := keyword_beg + 1; tok < keyword_end; tok++ {
		keywords[strings.ToLower(tokens[tok])] = tok
	}
	for tok := rule_attribute_beg + 1; tok < rule_attribute_end; tok++ {
		keywords[strings.ToLower(tokens[tok])] = tok
	}
	for _, tok := range []Token{AND, OR} {
		keywords[strings.ToLower(tokens[tok])] = tok
	}
	keywords["true"] = TRUE
	keywords["false"] = FALSE
}

// String returns the string representation of the token.
func (tok Token) String() string {
	if tok >= 0 && tok < Token(len(tokens)) {
		return tokens[tok]
	}
	return ""
}

// Precedence returns the operator precedence of the binary operator token.
func (tok Token) Precedence() int {
	switch tok {
	case OR:
		return 1
	case AND:
		return 2
	case EQ, NEQ, LT, LTE, GT, GTE:
		return 3
	case ADD, SUB:
		return 4
	case MUL, DIV:
		return 5
	}
	return 0
}

// isOperator returns true for operator tokens.
func (tok Token) isOperator() bool { return tok > operator_beg && tok < operator_end }

// tokstr returns a literal if provided, otherwise returns the token string.
func tokstr(tok Token, lit string) string {
	if lit != "" {
		return lit
	}
	return tok.String()
}

// Lookup returns the token associated with a given string.
func Lookup(ident string) Token {
	if tok, ok := keywords[strings.ToLower(ident)]; ok {
		return tok
	}
	return IDENT
}

func PossiblyAnAttribute(partialIdent string) bool {
	partialIdent = strings.ToLower(partialIdent)
	for tok := rule_attribute_beg + 1; tok < rule_attribute_end; tok++ {
		ident := strings.ToLower(tokens[tok])
		if strings.Index(ident, partialIdent) == 0 {
			return true
		}
	}
	return false
}

// Pos specifies the line and character position of a token.
// The Char and Line are both zero-based indexes.
type Pos struct {
	Line int
	Char int
}
