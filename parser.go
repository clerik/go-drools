package drools

import (
	"errors"
	"fmt"
	"io"
	"strings"
)

// Parser represents an mvel parser.
type Parser struct {
	s *bufScanner
}

// NewParser returns a new instance of Parser.
func NewParser(r io.Reader) *Parser {
	return &Parser{s: newBufScanner(r)}
}

func (p *Parser) ParseString(s *strings.Reader) (*Knowledge, error) {
	return p.parseKnowledge(s)
}

func (p *Parser) parseKnowledge(s *strings.Reader) (*Knowledge, error) {
	var rules Rules

FOR:
	for {
		tok, _, _ := p.scanIgnoreWhitespace()
		switch tok {
		case RULE:
			//p.unscan()
			r, err := p.parseRule()
			if err != nil {
				return nil, err
			}
			rules = append(rules, r)
		case EOF:
			break FOR
		}
	}

	return &Knowledge{Rules: rules}, nil
}

func (p *Parser) parseRule() (Rule, error) {
	var (
		predicates FactPredicates
		metas      RuleMetas
	)
	name, err := p.parseName()

	if err != nil {
		return Rule{}, err
	}

FOR:
	for {
		tok, _, _ := p.scanIgnoreWhitespace()
		switch tok {
		case META_IDENT:
			p.unscan()
			m, err := p.parseRuleMeta()
			if err != nil {
				return Rule{}, err
			}
			metas = append(metas, m)
			break
		case WHEN:
			predicates, err = p.parsePredicates()
		case THEN:
			// Do Something
		case END:
			break FOR
		case EOF:
			break FOR
		}
	}

	return Rule{Name: name, Predicates: predicates, Metas: metas}, nil
}

func (p *Parser) parseName() (string, error) {
	tok, _, lit := p.scanIgnoreWhitespace()

	if tok != IDENT && tok != STRING {
		return "", errors.New("Identifier expected, received " + tokens[tok])
	}

	return lit, nil
}

func (p *Parser) parsePredicates() (FactPredicates, error) {
	var predicates FactPredicates

FOR:
	for {
		tok, _, _ := p.scanIgnoreWhitespace()
		switch tok {
		case IDENT:
			p.unscan()
			f, err := p.parseFact()
			if err != nil {
				return nil, err
			}
			predicates = append(predicates, f)
		case THEN:
			break FOR
		case EOF:
			break FOR
		}
	}

	return predicates, nil
}

func (p *Parser) parseFact() (FactPredicate, error) {
	//	var expr string
	//	var expected Token
	//	expected = IDENT
	var (
		bindings    Bindings
		expressions Expressions
	)
	innerLevel := 0

	var idents []string
	var op Token

	name, err := p.parseName()
	if err != nil {
		return FactPredicate{}, err
	}

FOR:
	for {
		tok, _, lit := p.scanIgnoreWhitespace()
		//		fmt.Printf("Token %s -> %s\n", TokenToString(tok), lit)
		if isFactOperator(tok) && tok != COMMA {
			op = tok
		} else if tok >= IDENT && tok <= NULL {
			idents = append(idents, lit)
			if len(idents) == 2 {
				e := Expression{Lhs: idents[0], Op: op, Rhs: idents[1]}
				expressions = append(expressions, e)

				tok2, _, _ := p.scanIgnoreWhitespace()
				if op == COLON && tok2 != COMMA {
					n := idents[1]
					idents = make([]string, 0)
					idents = append(idents, n)
				} else {
					idents = make([]string, 0)
					op = 0
				}
				p.unscan()
			}
		} else {
			switch tok {
			case LPAREN:
				innerLevel += 1
			case RPAREN:
				innerLevel -= 1
				if innerLevel == 0 {
					break FOR
				}
			case COMMA:
				break
			case THEN:
				break FOR
			case EOF:
				break FOR
			}
		}
	}

	return FactPredicate{Name: name, Bindings: bindings, Expressions: expressions}, nil
}

func (p *Parser) parseRuleMeta() (RuleMeta, error) {
	var (
		name       string
		parameters []string
		innerLevel int
	)

	name, err := p.parseMetaIdent()
	innerLevel = 0

	if err != nil {
		return RuleMeta{}, err
	}

FOR:
	for {
		tok, _, _ := p.scanIgnoreWhitespace()
		if tok > litteral_beg && tok < litteral_end {
			p.unscan()
			param, err := p.parseSpecialIdent()
			if err != nil {
				return RuleMeta{}, err
			}
			parameters = append(parameters, param)
		} else {
			switch tok {
			case META_IDENT:
				p.unscan()
				break FOR
			case LPAREN:
				innerLevel += 1
			case RPAREN:
				innerLevel -= 1
				if innerLevel == 0 {
					break FOR
				}
			case WHEN:
				p.unscan()
				break FOR
			case END:
				break FOR
			case EOF:
				break FOR
			}
		}
	}

	return RuleMeta{Name: name, Parameters: parameters}, nil
}

func (p *Parser) parseMetaIdent() (string, error) {
	var (
		name string
		err  error
	)

	tok, _, lit := p.scan()
	//	fmt.Printf("Token %s -> %s\n", TokenToString(tok), lit)
	switch tok {
	case META_IDENT:
		name = lit
	default:
		err = errors.New(fmt.Sprintf("Token META_IDENT expected, received %s", TokenToString(tok)))
	}

	return name, err
}

func (p *Parser) parseSpecialIdent() (string, error) {
	var (
		name string
		err  error
	)

OUTTER:
	for {
		tok, _, lit := p.scan()
		//		fmt.Printf("Token %s -> %s\n", TokenToString(tok), lit)
		if tok > litteral_beg && tok < META_IDENT {
			name = name + lit
			//			fmt.Printf("name: %s\n", name)
		} else {
			switch tok {
			case WS:
				break OUTTER
			case COMMA:
				p.unscan()
				break OUTTER
			case RPAREN:
				break OUTTER
			case EOF:
				break OUTTER
			}
		}
	}

	return name, err
}

func isFactOperator(tok Token) bool {
	return tok > operator_beg && tok < operator_end
}

// scan returns the next token from the underlying scanner.
func (p *Parser) scan() (tok Token, pos Pos, lit string) { return p.s.Scan() }

// scanIgnoreWhitespace scans the next non-whitespace token.
func (p *Parser) scanIgnoreWhitespace() (tok Token, pos Pos, lit string) {
	tok, pos, lit = p.scan()
	if tok == WS {
		tok, pos, lit = p.scan()
	}
	return
}

// consumeWhitespace scans the next token if it's whitespace.
func (p *Parser) consumeWhitespace() {
	if tok, _, _ := p.scan(); tok != WS {
		p.unscan()
	}
}

// unscan pushes the previously read token back onto the buffer.
func (p *Parser) unscan() { p.s.Unscan() }

// ParseError represents an error that occurred during parsing.
type ParseError struct {
	Message  string
	Found    string
	Expected []string
	Pos      Pos
}

// newParseError returns a new instance of ParseError.
func newParseError(found string, expected []string, pos Pos) *ParseError {
	return &ParseError{Found: found, Expected: expected, Pos: pos}
}

// Error returns the string representation of the error.
func (e *ParseError) Error() string {
	if e.Message != "" {
		return fmt.Sprintf("%s at line %d, char %d", e.Message, e.Pos.Line+1, e.Pos.Char+1)
	}
	return fmt.Sprintf("found %s, expected %s at line %d, char %d", e.Found, strings.Join(e.Expected, ", "), e.Pos.Line+1, e.Pos.Char+1)
}
