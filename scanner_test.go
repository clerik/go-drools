package drools_test

import (
	"go-drools"
	"strings"
	"testing"
)

func TestScanner_Scan(t *testing.T) {
	var tests = []struct {
		s   string
		tok drools.Token
		lit string
		pos drools.Pos
	}{
		{s: ``, tok: drools.EOF},
		{s: `#`, tok: drools.ILLEGAL, lit: `#`},
		{s: ` `, tok: drools.WS, lit: " "},
		{s: "\t", tok: drools.WS, lit: "\t"},
		{s: "\n", tok: drools.WS, lit: "\n"},
		{s: "\r", tok: drools.WS, lit: "\n"},
		{s: "\r\n", tok: drools.WS, lit: "\n"},
		{s: "\rX", tok: drools.WS, lit: "\n"},
		{s: "\n\r", tok: drools.WS, lit: "\n\n"},
		{s: " \n\t \r\n\t", tok: drools.WS, lit: " \n\t \n\t"},
		{s: " foo", tok: drools.WS, lit: " "},

		// Numeric operators
		{s: `+`, tok: drools.ADD},
		{s: `-`, tok: drools.SUB},
		{s: `*`, tok: drools.MUL},
		{s: `/`, tok: drools.DIV},

		// Logical operators
		{s: `AND`, tok: drools.AND},
		{s: `and`, tok: drools.AND},
		{s: `OR`, tok: drools.OR},
		{s: `or`, tok: drools.OR},

		{s: `=`, tok: drools.ILLEGAL},
		{s: `==`, tok: drools.EQ},
		{s: `<>`, tok: drools.NEQ},
		{s: `! `, tok: drools.ILLEGAL, lit: "!"},
		{s: `<`, tok: drools.LT},
		{s: `<=`, tok: drools.LTE},
		{s: `>`, tok: drools.GT},
		{s: `>=`, tok: drools.GTE},

		// Misc tokens
		{s: `(`, tok: drools.LPAREN},
		{s: `)`, tok: drools.RPAREN},
		{s: `,`, tok: drools.COMMA},
		{s: `;`, tok: drools.SEMICOLON},
		{s: `.`, tok: drools.DOT},
		{s: `// something`, tok: drools.COMMENT, lit: ` something`, pos: drools.Pos{Line: 0, Char: 2}},
		{s: `/* something else */`, tok: drools.COMMENT, lit: ` something else `, pos: drools.Pos{Line: 0, Char: 2}},

		// Identifiers
		{s: `foo`, tok: drools.IDENT, lit: `foo`},
		{s: `_foo`, tok: drools.IDENT, lit: `_foo`},
		{s: `Zx12_3U_-`, tok: drools.IDENT, lit: `Zx12_3U_`},
		{s: `$var1`, tok: drools.IDENT, lit: `$var1`},
		{s: `test"`, tok: drools.BADSTRING, lit: "", pos: drools.Pos{Line: 0, Char: 3}},
		{s: `"test`, tok: drools.BADSTRING, lit: `test`},

		// Meta Identifiers
		{s: `@key`, tok: drools.META_IDENT, lit: `key`, pos: drools.Pos{Line: 0, Char: 1}},
		{s: `@role ( event )`, tok: drools.META_IDENT, lit: `role`, pos: drools.Pos{Line: 0, Char: 1}},
		{s: `@role(fact)`, tok: drools.META_IDENT, lit: `role`, pos: drools.Pos{Line: 0, Char: 1}},

		{s: `true`, tok: drools.TRUE},
		{s: `false`, tok: drools.FALSE},

		// Strings
		{s: `'testing 123!'`, tok: drools.STRING, lit: `testing 123!`},
		{s: `'foo\nbar'`, tok: drools.STRING, lit: "foo\nbar"},
		{s: `'foo\\bar'`, tok: drools.STRING, lit: "foo\\bar"},
		{s: `'test`, tok: drools.BADSTRING, lit: `test`},
		{s: "'test\nfoo", tok: drools.BADSTRING, lit: `test`},
		{s: `'test\g'`, tok: drools.BADESCAPE, lit: `\g`, pos: drools.Pos{Line: 0, Char: 6}},
		{s: `"foo"`, tok: drools.STRING, lit: `foo`},
		{s: `"foo\\bar"`, tok: drools.STRING, lit: `foo\bar`},
		{s: `"foo\bar"`, tok: drools.BADESCAPE, lit: `\b`, pos: drools.Pos{Line: 0, Char: 5}},
		{s: `"foo\"bar\""`, tok: drools.STRING, lit: `foo"bar"`},

		// Numbers
		{s: `100`, tok: drools.NUMBER, lit: `100`},
		{s: `100.23`, tok: drools.NUMBER, lit: `100.23`},
		{s: `+100.23`, tok: drools.NUMBER, lit: `+100.23`},
		{s: `-100.23`, tok: drools.NUMBER, lit: `-100.23`},
		{s: `-100.`, tok: drools.NUMBER, lit: `-100`},
		{s: `.23`, tok: drools.NUMBER, lit: `.23`},
		{s: `+.23`, tok: drools.NUMBER, lit: `+.23`},
		{s: `-.23`, tok: drools.NUMBER, lit: `-.23`},
		//{s: `.`, tok: drools.ILLEGAL, lit: `.`},
		{s: `-.`, tok: drools.SUB, lit: ``},
		{s: `+.`, tok: drools.ADD, lit: ``},
		{s: `10.3s`, tok: drools.NUMBER, lit: `10.3`},
		{s: `accumulate`, tok: drools.ACCUMULATE},
		{s: `ACCUMULATE`, tok: drools.ACCUMULATE},
		{s: `action`, tok: drools.ACTION},
		{s: `ATTRIBUTES`, tok: drools.ATTRIBUTES},
		{s: `COLLECT`, tok: drools.COLLECT},
		{s: `DECLARE`, tok: drools.DECLARE},
		{s: `DIALECT`, tok: drools.DIALECT},
		{s: `END`, tok: drools.END},
		{s: `ENTRY_POINT`, tok: drools.ENTRY_POINT},
		{s: `EVAL`, tok: drools.EVAL},
		{s: `EXISTS`, tok: drools.EXISTS},
		{s: `EXTEND`, tok: drools.EXTEND},
		{s: `FORALL`, tok: drools.FORALL},
		{s: `FROM`, tok: drools.FROM},
		{s: `FUNCTION`, tok: drools.FUNCTION},
		{s: `GLOBAL`, tok: drools.GLOBAL},
		{s: `IMPORT`, tok: drools.IMPORT},
		{s: `IN`, tok: drools.IN},
		{s: `INIT`, tok: drools.INIT},
		{s: `NOT`, tok: drools.NOT},
		{s: `OVER`, tok: drools.OVER},
		{s: `PACKAGE`, tok: drools.PACKAGE},
		{s: `QUERY`, tok: drools.QUERY},
		{s: `RESULT`, tok: drools.RESULT},
		{s: `REVERSE`, tok: drools.REVERSE},
		{s: `RULE`, tok: drools.RULE},
		{s: `TEMPLATE`, tok: drools.TEMPLATE},
		{s: `THEN`, tok: drools.THEN},
		{s: `WHEN`, tok: drools.WHEN},

		{s: `ACTIVATION-GROUP`, tok: drools.ACTIVATION_GROUP},
		{s: `AGENDA-GROUP`, tok: drools.AGENDA_GROUP},
		{s: `AUTO-FOCUS`, tok: drools.AUTO_FOCUS},
		{s: `DATE-EFFECTIVE`, tok: drools.DATE_EFFECTIVE},
		{s: `DATE-EXPIRES`, tok: drools.DATE_EXPIRES},
		{s: `DURATION`, tok: drools.DURATION},
		{s: `ENABLED`, tok: drools.ENABLED},
		{s: `LOCK-ON-ACTIVE`, tok: drools.LOCK_ON_ACTIVE},
		{s: `NO-LOOP`, tok: drools.NO_LOOP},
		{s: `RULEFLOW-GROUP`, tok: drools.RULEFLOW_GROUP},
		{s: `SALIENCE`, tok: drools.SALIENCE},
		{s: `attributes`, tok: drools.ATTRIBUTES},
		{s: `collect`, tok: drools.COLLECT},
		{s: `declare`, tok: drools.DECLARE},
		{s: `dialect`, tok: drools.DIALECT},
		{s: `end`, tok: drools.END},
		{s: `entry_point`, tok: drools.ENTRY_POINT},
		{s: `eval`, tok: drools.EVAL},
		{s: `exists`, tok: drools.EXISTS},
		{s: `extend`, tok: drools.EXTEND},
		{s: `forall`, tok: drools.FORALL},
		{s: `from`, tok: drools.FROM},
		{s: `function`, tok: drools.FUNCTION},
		{s: `global`, tok: drools.GLOBAL},
		{s: `import`, tok: drools.IMPORT},
		{s: `in`, tok: drools.IN},
		{s: `init`, tok: drools.INIT},
		{s: `not`, tok: drools.NOT},
		{s: `over`, tok: drools.OVER},
		{s: `package`, tok: drools.PACKAGE},
		{s: `query`, tok: drools.QUERY},
		{s: `result`, tok: drools.RESULT},
		{s: `reverse`, tok: drools.REVERSE},
		{s: `rule`, tok: drools.RULE},
		{s: `template`, tok: drools.TEMPLATE},
		{s: `then`, tok: drools.THEN},
		{s: `when`, tok: drools.WHEN},
		{s: `activation-group`, tok: drools.ACTIVATION_GROUP},
		{s: `agenda-group`, tok: drools.AGENDA_GROUP},
		{s: `auto-focus`, tok: drools.AUTO_FOCUS},
		{s: `date-effective`, tok: drools.DATE_EFFECTIVE},
		{s: `date-expires`, tok: drools.DATE_EXPIRES},
		{s: `duration`, tok: drools.DURATION},
		{s: `enabled`, tok: drools.ENABLED},
		{s: `lock-on-active`, tok: drools.LOCK_ON_ACTIVE},
		{s: `no-loop`, tok: drools.NO_LOOP},
		{s: `ruleflow-group`, tok: drools.RULEFLOW_GROUP},
		{s: `salience`, tok: drools.SALIENCE},
	}

	for i, tt := range tests {
		s := drools.NewScanner(strings.NewReader(tt.s))
		tok, pos, lit := s.Scan()
		if tt.tok != tok {
			t.Errorf("%d. %q token mismatch: exp=%q got=%q <%q>", i, tt.s, tt.tok, tok, lit)
		} else if tt.pos.Line != pos.Line || tt.pos.Char != pos.Char {
			t.Errorf("%d. %q pos mismatch: exp=%#v got=%#v", i, tt.s, tt.pos, pos)
		} else if tt.lit != lit {
			t.Errorf("%d. %q literal mismatch: exp=%q got=%q", i, tt.s, tt.lit, lit)
		}
	}
}
