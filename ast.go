package drools

type Property struct {
	Name  string
	Type  string
	IsKey bool
	// Meta
}

type Properties []Property

type Definition struct {
	Name       string
	Extend     string
	Properties Properties
	// Meta
}

type Definitions []Definition

type FunctionParameter struct {
	Name string
	Type string
}

type FunctionParameters []FunctionParameter

type Function struct {
	Name       string
	Params     FunctionParameters
	ReturnType string
}

type Functions []Function

type Expression struct {
	Lhs string
	Op  Token
	Rhs string
}

type Expressions []Expression

type Binding struct {
	Name         string
	PropertyName string
	//	Property Property
}

type Bindings []Binding

type FactPredicate struct {
	Name        string
	Bindings    Bindings
	Expressions Expressions
}

type FactPredicates []FactPredicate

type RuleMeta struct {
	Name       string
	Parameters []string
}

type RuleMetas []RuleMeta

type Rule struct {
	Name       string
	Metas      RuleMetas
	Predicates FactPredicates
}

type Rules []Rule

type Knowledge struct {
	Rules       Rules
	Functions   Functions
	Definitions Definitions
}
